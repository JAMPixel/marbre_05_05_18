#include "army.hpp"
#include "unitstate.hpp"
#include <iostream>
#include <cmath>

Army::Army(Player team, Pos pos)
{
  _owner = team;
  _currentPos = pos;
  _state = WAITING;
}

int Army::size()
{
  return _units.size();
}

void Army::addUnit(Unit * unit)
{
  _units.push_back(unit);
}

void Army::shuffle()
{
  int length = size();
  for (int i = 0; i < (length - 1); ++i)
  {
    int j = rand() % (length - i - 1) + i + 1;
    Unit * tmp = _units[i];
    _units[i] = _units[j];
    _units[j] = tmp;
  }
}

void Army::setTarget(Pos pos)
{
  _target = pos;
  setState(WALKING);
}

void Army::setState(ArmyState state)
{
  _state = state;
  UnitState unitState;
  if (state == WAITING)
  {
    unitState = STANDBY;
  }
  else if (state == ATTACKING)
  {
    unitState = FIGHTING;
  }
  else if (state == WALKING)
  {
    unitState = RUNNING;
  }
  if (state != MERGING)
  {
    int length = size();
    for (int i = 0; i < length; ++i)
    {
      _units[i]->setState(unitState);
    }
  }
}

void Army::merge(Army * army)
{
  int length = army->size();
  for (int i = 0; i < length; ++i)
  {
    addUnit(army->getUnit(i));
  }
  setState(ATTACKING);
}

void Army::walk()
{
  int length = size();
  float speed = _units[0]->getSpeed();
  _units[0]->walk(_target);

  std::cerr << "TA MERE" << "\n";


  for (int i = 1; i < length; ++i)
  {
    float tmp = _units[i]->getSpeed();
    if (tmp < speed)
    {
      speed = tmp;
    }
    _units[i]->walk(_target);
  }

  float newX = _target.x - _currentPos.x;
  float newY = _target.y - _currentPos.y;
  if (newX == 0)
  {
    _currentPos.y += speed * newY;
  }
  else if (newX > 0) {
    _currentPos.x += speed * cos(atan(newY/newX));
    _currentPos.y += speed * sin(atan(newY/newX));
  }
  else
  {
    _currentPos.x -= speed * cos(atan(newY/newX));
    _currentPos.y -= speed * sin(atan(newY/newX));
  }

  if (((int) round(_currentPos.x)) == ((int) round(_target.x)) && (int) round(_currentPos.y) == ((int) round(_target.y)))
  {
    _currentPos.x = _target.x;
    _currentPos.y = _target.y;

    setState(MERGING);

  }
}

void Army::attack(Army * opponent)
{
  int i = 0;
  int selfSize = size();
  int opponentSize = opponent->size();
  bool opponentIsDead = false;
  while (i < selfSize && opponentIsDead)
  {
    int j = 0;
    opponent->shuffle();
    while (j < opponentSize && opponent->getUnit(j)->isDead())
    {
      ++j;
    }
    if (j == opponentSize)
    {
      opponentIsDead = true;
    }
    else
    {
      _units[i]->fight(opponent->getUnit(j));
    }
  }
  if (opponentIsDead)
  {
    setState(WAITING);
  }
}

void Army::update()
{
  switch(_state)
    {
    case WALKING:
      walk();
      break;
    default:
      break;
    }

}
