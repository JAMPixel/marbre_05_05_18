#ifndef ARMY_H
#define ARMY_H

#include <vector>
#include "unit.hpp"
#include "player.hpp"
#include "pos.hpp"
#include "armystate.hpp"

class Army
{
private:
  std::vector<Unit *> _units;
  Pos                 _currentPos;
  Pos                 _target;
  Player              _owner;
  ArmyState           _state;
public:
  Army(){}
  Army(Player team, Pos pos);
  int size();

  void addUnit(Unit * unit);
  Unit * getUnit(int i) { return _units[i]; }
  ArmyState getState() { return _state; }
  Player getOwner() { return _owner; }
  void shuffle();
  void setTarget(Pos pos);
  Pos  getTarget() { return _target; }
  void setState(ArmyState state);
  void merge(Army * army);
  void walk();
  void attack(Army * opponent);
  void update();
};

#endif /* ARMY_H */
