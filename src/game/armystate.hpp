#ifndef ARMYSTATE_H
#define ARMYSTATE_H

typedef enum {WAITING, WALKING, MERGING, ATTACKING} ArmyState;

#endif /* ARMYSTATE_H */
