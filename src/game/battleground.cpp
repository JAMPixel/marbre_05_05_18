#include "battleground.hpp"
#include "game.hpp"
#include "screen.h"
#include <iostream>

bool operator!=(const Pos& p1, const Pos& p2)
{
  return p1.x != p2.x || p1.y != p2.y;
}

bool operator!=(Unit& u1, Unit& u2)
{
  return !(u1.owner == u2.owner && !(u1.getPos() != u2.getPos()));
}

Battleground::Battleground()
{
  _selected1 = NULL;
  _selected2 = NULL;
  _currentFlagHeaven = HEAVEN;
  _currentFlagHell = HELL;
  Pos p = {0, HEIGHT / 2 - HEIGHT_F, WIDTH_F, HEIGHT_F};
  Army * hellArmy = new Army(HELL, p);
  p = {WIDTH - WIDTH_F, HEIGHT / 2 - HEIGHT_F, WIDTH_F, HEIGHT_F};
  Army * heavenArmy = new Army(HEAVEN, p);
  addArmy(hellArmy);
  addArmy(heavenArmy);
  _hellTrainingArmy = hellArmy;
  _heavenTrainingArmy = heavenArmy;
  /*setTrainingArmy(HELL, hellArmy);
    setTrainingArmy(HEAVEN, heavenArmy);*/
}

void Battleground::addFlag(Flag flag, Element * elt)
{
  _flags.push_back(flag);
  _flagsSDL.push_back(elt);
}

void Battleground::addArmy(Army * army)
{
  _armies.push_back(army);
}

Army * Battleground::getTrainingArmy(Player team)
{
  return (team == HELL)?_hellTrainingArmy:_heavenTrainingArmy;
}

void Battleground::setTrainingArmy(Player team, Army * army)
{
  if (team == HELL)
  {
    _flags[HELL].setArmy(army, HELL);
    _hellTrainingArmy = army;
  }
  if (team == HEAVEN)
  {
    _flags[HEAVEN].setArmy(army, HEAVEN);
    _heavenTrainingArmy = army;
  }

  addArmy(army);
}

/**
 *\brief Update all the objects and set the new settings to SANDAL 2
 */

void Battleground::update()
{
  int blue[4]   = {0,0,255,0};
  int purple[4] = {155,0,255,0};
  int red[4]    = {255,0,0,0};
  int grey[4]   = {127,127,127,0};
  int yellow[4] = {255,255,0,0};
  int black[4]  = {0,0,0,0};
  int orange[4] = {253,153,0,0};
  int white[4]  = {255,255,255,0};

  /* Update the flag */

  for (unsigned int i = 0; i < _flags.size(); ++i)
  {
    _flags[i].update();

    if(i == 0 && _flags[i].owner != HEAVEN)
      _flags[i].owner = HELL;

    if(i == 1 && _flags[i].owner != HELL)
      _flags[i].owner = HEAVEN;

    switch(_flags[i].owner)
      {
      case HEAVEN:
	setColorElement(_flagsSDL[i], blue);
	break;
      case HELL:
	setColorElement(_flagsSDL[i], red);
	break;
      case NEUTRAL:
	setColorElement(_flagsSDL[i], grey);
	break;
      }
  }

  /* Update the flag cursor */

  //todo : make a method to avoid duplication

  if(Game::joystick1.right && !(Game::joystick1.stick_release = false))
    _currentFlagHell = (_currentFlagHell + 1)%(_flags.size());

  if(Game::joystick1.left && !(Game::joystick1.stick_release = false))
    {
      --_currentFlagHell;
      if(_currentFlagHell < 0)
	_currentFlagHell += _flags.size();
    }

  if(Game::joystick2.right && !(Game::joystick2.stick_release = false))
    _currentFlagHeaven = (_currentFlagHeaven + 1)%(_flags.size());

  if(Game::joystick2.left && !(Game::joystick2.stick_release = false))
    {
      --_currentFlagHeaven;
      if(_currentFlagHeaven < 0)
	_currentFlagHeaven += _flags.size();
    }

  Game::joystick2.left = Game::joystick2.right = Game::joystick1.left = Game::joystick1.right = false;

  setColorElement(_flagsSDL[_currentFlagHeaven], purple);
  setColorElement(_flagsSDL[_currentFlagHell], yellow);

  /* Selection */

  //todo : faire une fonction pour ne pas dédoubler le code

  if(Game::joystick1.ok)
    {
      Pos pos = _flags[_currentFlagHell].getPosition();

      if(_selected1 != NULL)
	{
	  if(pos != _selectedFlag1->getPosition())
	    {
	      delElement(_selected1);
	      _selected1 = NULL;
	      _selectedFlag1->getHell()->setTarget(pos);

	      //Pos p = {0, HEIGHT / 2 - HEIGHT_F, WIDTH_F, HEIGHT_F};
	      Army * newArmy = new Army(HELL, _selectedFlag1->getPosition());
	      _selectedFlag1->setArmy(newArmy, HELL);
	      _armies.push_back(newArmy);
	      _armies.push_back(_flags[_currentFlagHell].getHell());
	      if(!(_selectedFlag1->getPosition() != _flags[HELL].getPosition()))
		_hellTrainingArmy = newArmy;

	    }
	}
      else
	{
	  _selected1 = createBlock(pos.x - 10, pos.y - 10, pos.w + 2*10, pos.h + 2*10, black, GAME, 7);
	  _selectedFlag1 = &_flags[_currentFlagHell];
	}

      Game::joystick1.ok = false;
      Game::joystick1.select = true;
    }

  if(Game::joystick2.ok)
    {
      Pos pos = _flags[_currentFlagHeaven].getPosition();
      if(_selected2 != NULL)
	{
	  if(pos != _selectedFlag2->getPosition())
	    {
	      delElement(_selected2);
	      _selected2 = NULL;
	      _selectedFlag2->getHeaven()->setTarget(pos);

	      Army * newArmy = new Army(HELL, _selectedFlag2->getPosition());
	      
	      _selectedFlag2->setArmy(newArmy, HEAVEN);
	      _armies.push_back(newArmy);
	      _armies.push_back(_flags[_currentFlagHeaven].getHeaven());
	      if(!(_selectedFlag2->getPosition() != _flags[HEAVEN].getPosition()))
		_heavenTrainingArmy = newArmy;
	    }
	}
      else
	{
	  _selected2 = createBlock(pos.x - 10, pos.y - 10, pos.w + 2*10, pos.h + 2*10, black, GAME, 7);
	  _selectedFlag2 = &_flags[_currentFlagHeaven];
	}

      Game::joystick2.ok = false;
      Game::joystick2.select = true;
    }

  /* Create units */

  if(Game::joystick1.create)
    {
      Game::joystick1.create = false;
      Game::joystick1.select = true;

      Unit * unit = new Unit(HELL);
      Pos pos = _flags[HELL].getPosition();

      unit->setPos(pos);
      _units.push_back(unit);
      _unitsSDL.push_back(createBlock(pos.x, pos.y, pos.w / 4, pos.h / 4, orange, GAME, 1));
    }

  if(Game::joystick2.create)
    {
      Game::joystick2.create = false;
      Game::joystick2.select = true;

      Unit * unit = new Unit(HEAVEN);
      Pos pos = _flags[HEAVEN].getPosition();

      unit->setPos(pos);
      _units.push_back(unit);
      _unitsSDL.push_back(createBlock(pos.x, pos.y, pos.w / 4, pos.h / 4, orange, GAME, 1));
    }


  /* Update armies */

   for(unsigned int i = 0; i < _armies.size() ; ++i)
    {
      _armies[i]->update();
    }

   for (int i = _armies.size()-1; i >= 0; --i)
     {
       switch (_armies[i]->getState())
	 {
	 case MERGING:
	   {
	   int j = 0;
	   Flag f = _flags[j];
	   while (f.getPosition() != _armies[i]->getTarget())
	     {
	       j++;
	       f = _flags[j];
	     }
	   if (_armies[i]->getOwner() == HELL)
	     {
	       f.getHell()->merge(_armies[i]);
         for (unsigned int  k = 0; k < _units.size(); ++k)
         {
           int l = 0;
           while (l < _armies[i]->size() && _armies[i]->getUnit(l) != _units[k])
           {
             l++;
           }
           if (l < _armies[i]->size())
           {
             _units[k] = f.getHell()->getUnit(l);
           }
         }
	     }
	   else
	     {
	       f.getHeaven()->merge(_armies[i]);
         for (unsigned int  k = 0; k < _units.size(); ++k)
         {
           int l = 0;
           while (l < _armies[i]->size() && *_armies[i]->getUnit(l) != *_units[k])
           {
             l++;
           }
           if (l < _armies[i]->size())
           {
             _units[k] = f.getHeaven()->getUnit(l);
           }
         }
	     }
	   _armies.erase(_armies.begin() + i);
	   break;
	   }
	 default:
	   break;
	 }
     }

  /* Update all the units */

  for(int i = _units.size() - 1; i >= 0; i--)
    {
      if(_units[i]->getState() == TRAINING)
        {
	      _units[i]->training();
	      if(_units[i]->getTime() <= 0)
	        {
	        setColorElement(_unitsSDL[i], (_units[i]->owner == HELL)?black:white);
	        if(_units[i]->owner == HELL)
	        	{
		        _hellTrainingArmy->addUnit(_units[i]);
            _hellTrainingArmy->setState(_hellTrainingArmy->getState());
	        	}
	        else
	          _heavenTrainingArmy->addUnit(_units[i]);
            _heavenTrainingArmy->setState(_heavenTrainingArmy->getState());

    	  }
      }
      else
        {
	      Pos pos = _units[i]->getPos();

	      replaceElement(_unitsSDL[i], pos.x , pos.y);

	      if(_units[i]->getLife() <= 0)

	        {

	        _units.erase(_units.begin() + i);
	        delElement(_unitsSDL[i]);
	        _unitsSDL.erase(_unitsSDL.begin() + i);
	        }
	      }
    }
}
