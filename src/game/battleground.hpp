#ifndef BATTLEGROUND_H
#define BATTLEGROUND_H

#include <SANDAL2/SANDAL2.h>
#include <vector>
#include "flag.hpp"
#include "army.hpp"
#include "player.hpp"

/**
 *\brief Where all the actions are handled
 */

class Battleground
{
private:
  std::vector<Flag>      _flags; //mainFlag are at 0 (HELL) and 1 (HEAVEN)
  std::vector<Element *> _flagsSDL;
  std::vector<Army *>    _armies;
  Army                 * _hellTrainingArmy;
  Army                 * _heavenTrainingArmy;
  std::vector<Unit *>    _units;
  std::vector<Element *> _unitsSDL;
  int                    _currentFlagHeaven;
  int                    _currentFlagHell;
  Element              * _selected1, * _selected2;
  Flag                 * _selectedFlag1, * _selectedFlag2;

public:
  Battleground();
  void addFlag(Flag flag, Element * elt);
  void addArmy(Army * army);
  Army * getTrainingArmy(Player team);
  void setTrainingArmy(Player team, Army * army);
  void update();
};

#endif /* BATTLEGROUND_H */
