#include "flag.hpp"

Flag::Flag()
{
  _pos.x = 0;
  _pos.y = 0;
  _pos.w = 0;
  _pos.h = 0;
  owner = NEUTRAL;
}

Flag::Flag(Pos position)
{
  owner = NEUTRAL;
  _pos = position;
  _heaven = new Army(HEAVEN, position);
  _hell = new Army(HELL, position);
}

Flag::Flag(Pos position, Player team, Army * army)
{
  if (team == HELL)
  {
    _hell = army;
    _heaven = new Army(HEAVEN, position);
  }
  else
  {
    _heaven = army;
    _hell = new Army(HELL, position);
  }
  owner = team;
  _pos = position;
}

void Flag::update()
{
  if(_heaven->size() > _hell->size())
    owner = HEAVEN;
  else if(_heaven->size() < _hell->size())
    owner = HELL;
  else
    owner = NEUTRAL;
  if (_heaven->size() != 0)
  {
    _hell->setState(ATTACKING);
    _hell->attack(_heaven);
  }
  if (_hell->size() != 0)
  {
    _heaven->setState(ATTACKING);
    _heaven->attack(_hell);
  }
}

void Flag::setArmy(Army * army, Player team)
{
  if(team == HELL)
    {
      _hell = army;
    }
  else
    {
      _heaven = army;
    }
}
