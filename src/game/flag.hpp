#ifndef FLAG_H
#define FLAG_H

#include "pos.hpp"
#include "player.hpp"
#include "army.hpp"

/**
 *\brief The flag is the place to control
 */

class Flag
{
private:
  Pos    _pos;
  Army * _heaven;
  Army * _hell;
public:
  Player owner;
  Flag();
  Flag(Pos position);
  Flag(Pos position, Player team, Army * army);
  void update();
  void setArmy(Army * army, Player team);
  Pos getPosition() { return _pos; }
  Army * getHeaven() { return _heaven; }
  Army * getHell() { return _hell; }
};

#endif /* FLAG_H */
