#include <SANDAL2/SANDAL2.h>
#include <iostream>
#include "game.hpp"
#include "screen.h"
#include "pos.hpp"
#include "flag.hpp"
#include "player.hpp"

Joystick Game::joystick1;
Joystick Game::joystick2;

void Game::run()
{
  int    run = 1;
  int    tps, ticks;
  int  * close;

  while(run)
    {
      tps = SDL_GetTicks();
      /* gestion d'evenement */
      run=!PollEvent(NULL);
      /* update de la fenetre */
      Game::joystick1.update();
      Game::joystick2.update();

      _bg.update();

      updateWindow();
      /* affichage de la fenetre */
      displayWindow();
      /* delai pour 60 frames/secondes */
      ticks = 16 - SDL_GetTicks() + tps;
      if(ticks>0)
	SDL_Delay(ticks);

      if(initIteratorWindow())
	run = 0;

      getDataWindow((void **)&close);

      if(*close)
	{
	  run = 0;
	}
    }
}

/**
 *\brief load the battleground and place all the flags on it
 */

void Game::init(int nb_flag)
{
  Element      * flagSDL;
  int            green[4] = {0,255,0,0};
  int            grey[4]  = {127,127,127,0};
  int            red[4] = {255,0,0,0};
  int            blue[4] = {0,0,255,0};
  float        * xlist = (float *) malloc(sizeof(float) * nb_flag);
  float        * ylist = (float *) malloc(sizeof(float) * nb_flag);

  Game::joystick1.init(0);
  Game::joystick2.init(1);

  createBlock(0,0,WIDTH,HEIGHT, green, GAME, 10);

  Pos p = {0, HEIGHT / 2 - HEIGHT_F, WIDTH_F, HEIGHT_F};
  Flag hellFlag(p, HELL, _bg.getTrainingArmy(HELL));
  flagSDL = createBlock(p.x, p.y, p.w, p.h, red, GAME, 5);
  _bg.addFlag(hellFlag, flagSDL);
  p = {WIDTH - WIDTH_F, HEIGHT / 2 - HEIGHT_F, WIDTH_F, HEIGHT_F};
  Flag heavenFlag(p, HEAVEN, _bg.getTrainingArmy(HEAVEN));
  flagSDL = createBlock(p.x, p.y, p.w, p.h, blue, GAME, 5);
  _bg.addFlag(heavenFlag, flagSDL);

  for (int i = 0; i < nb_flag; i++)
    {
      float x,y;
      bool posOk;

      do {
	posOk = true;
        x = rand() % (WIDTH - 3 * WIDTH_F) + 2 * WIDTH_F;
        y = rand() % (HEIGHT - 2 * HEIGHT_F) + HEIGHT_F;
        int j = 0;

        while (j < i && posOk)
	  {
	    posOk = (x < xlist[j] - 1.5 * WIDTH_F || x > xlist[j] + 1.5 * WIDTH_F || y < ylist[j] - 1.5 * HEIGHT_F || y > ylist[j] + 1.5 * HEIGHT_F) && posOk;
	    ++j;
	  }

      } while(!posOk);

      xlist[i] = x;
      ylist[i] = y;

      Pos  p = {x, y, WIDTH_F, HEIGHT_F};
      Flag flag(p);

      flagSDL = createBlock(p.x, p.y, p.w, p.h, grey, GAME, 5);

      _bg.addFlag(flag, flagSDL);
    }
}
