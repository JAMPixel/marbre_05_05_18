#ifndef GAME_H
#define GAME_H

#include "joystick.hpp"
#include "battleground.hpp"

#define WIDTH_F 50
#define HEIGHT_F 50

/**
 *\brief Class which will init and run the game
*/

class Game
{
private:
  Battleground _bg;
public:
  static Joystick joystick1;
  static Joystick joystick2;

  Game(){}
  void     run();
  void     init(int);
  virtual ~Game(){}
};


#endif /* GAME_H */
