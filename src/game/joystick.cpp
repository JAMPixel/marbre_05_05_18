#include "joystick.hpp"
#include <iostream>

Joystick::Joystick()
{
  stick_release = true;
  right = left = ok = split = false;
}

void Joystick::init(const unsigned int n)
{
  if(n < (unsigned int)SDL_NumJoysticks())
    {
      _joystick = SDL_JoystickOpen(n);

      if(!_joystick)
	puts("Error opening joystick");
    }
}

void Joystick::update()
{
  select = select && (SDL_JoystickGetButton(_joystick, 1) || SDL_JoystickGetButton(_joystick, 2));
  create = SDL_JoystickGetButton(_joystick, 2)  &&  !select;
  ok = SDL_JoystickGetButton(_joystick, 1) && !select;
  stick_release = stick_release || SDL_JoystickGetAxis(_joystick, 0) == 0;
  right = SDL_JoystickGetAxis(_joystick, 0) >= 32000 && !left && stick_release;
  left  = SDL_JoystickGetAxis(_joystick, 0) <= -32000 && !right && stick_release;
}

Joystick::~Joystick()
{
  SDL_JoystickClose(_joystick);
}
