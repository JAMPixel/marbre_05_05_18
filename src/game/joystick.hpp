#ifndef JOYSTICK_H
#define JOYSTICK_H

#include <SANDAL2/SANDAL2.h>

class Joystick
{
private:
  SDL_Joystick * _joystick;
public:
  bool right;
  bool left;
  bool ok;
  bool split;
  bool stick_release;
  bool select;
  bool create;
  Joystick();
  void update();
  void init(const unsigned int n);
  virtual ~Joystick();
};


#endif /* JOYSTICK_H */
