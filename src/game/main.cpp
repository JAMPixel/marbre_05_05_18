#include <iostream>
#include <time.h>

#include "screen.h"
#include "game.hpp"

void exit(SDL_Keycode c)
{
  if(c == SDLK_ESCAPE)
    {
      shouldCloseWindow();
      int * close;
      getDataWindow((void **)&close);
      *close = 1;
    }
}

int main(int argc, char *argv[])
{
  Game game;
  int  black[4] = {0,0,0,0};
  int  close;

  close = 0;

  srand(time(NULL));
  
  if(initAllSANDAL2(IMG_INIT_JPG)){
    puts("Failed to init SANDAL2");
    return -1;
  }

  if(SDL_Init(SDL_INIT_JOYSTICK) < 0)
    {
      puts("error joystick");
    }

  createWindow(WIDTH, /* width of the window */
	       HEIGHT, /* height of the window */
	       "Game", /* title */
	       0, /* Options (see below) */
	       black, /* background color */
	       GAME); /* Default display code of the window (we will see more about them later) */

  if(initIteratorWindow()){
    closeAllSANDAL2();
    fprintf(stderr,"Erreur d'ouverture de la fenetre.\n");
    exit(-1);
  }

  std::cout << SDL_NumJoysticks() << "\n";
  
  setDataWindow(&close);

  setKeyPressedWindow(exit);
  
  game.init(3);
  game.run();

  closeAllWindow(); /* close all windows */
  closeAllSANDAL2();
  
  return 0;
}
