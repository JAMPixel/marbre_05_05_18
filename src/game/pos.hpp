#ifndef POS_H
#define POS_H

typedef struct
{
  float x;
  float y;
  float w;
  float h;
} Pos;

#endif /* POS_H */
