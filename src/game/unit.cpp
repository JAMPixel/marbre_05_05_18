#include "unit.hpp"
#include <cmath>
#include <iostream>


Unit::Unit(Player team)
{
  owner = team;
  _life = 3;
  _speed = 1;
  _time = TRAINING_TIME;
  _state = TRAINING;
}

/*
Unit::Unit(int l, int str, float s, int ct)
{
  _life = l;
  _strength = str;
  _speed = s;
  _time = t;
  _state = TRAINING;
  }*/

float Unit::getSpeed()
{
  return _speed;
}

void Unit::setState(UnitState s)
{
  _state = s;
}

void Unit::training()
{
  if (--_time == 0)
  {
    setState(STANDBY);
  }
}

void Unit::walk(Pos target)
{
  float newX = target.x - _pos.x;
  float newY = target.y - _pos.y;
  if (newX == 0)
  {
    _pos.y += _speed * newY;
  }
  else if (newX > 0) {
    _pos.x += _speed * cos(atan(newY/newX));
    _pos.y += _speed * sin(atan(newY/newX));
  }
  else
  {
    _pos.x -= _speed * cos(atan(newY/newX));
    _pos.y -= _speed * sin(atan(newY/newX));
  }
}

void Unit::takeDamage(int str)
{
  _life -= str;
}

void Unit::fight(Unit * opponent)
{
  opponent->takeDamage(_strength);
}

void Unit::update()
{

}
