#ifndef UNIT_H
#define UNIT_H

#include "pos.hpp"
#include "unitstate.hpp"
#include "player.hpp"

#define TRAINING_TIME 500;

class Unit
{
private:
  int       _life;
  int       _strength;
  float     _speed;
  int       _time;
  Pos       _pos;
  UnitState _state;
  
public:
  Player owner;
  
  Unit(Player owner);
  float getSpeed();
  void setState(UnitState s);
  UnitState getState() { return _state; }
  void training();
  void walk(Pos target);
  void takeDamage(int str);
  void fight(Unit * opponent);
  void update();
  int  getLife(){ return _life; }
  bool isDead(){ return _life < 1; }
  int  getTime() { return _time; }
  Pos  getPos() { return _pos; }
  void setPos(Pos pos) { _pos = pos; }
};

#endif /* UNIT_H */
