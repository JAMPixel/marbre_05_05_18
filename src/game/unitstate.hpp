#ifndef UNITSTATE_H
#define UNITSTATE_H

typedef enum {TRAINING, STANDBY, RUNNING, FIGHTING} UnitState;

#endif /* UNITSTATE_H */
